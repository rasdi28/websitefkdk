-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 11, 2019 at 01:27 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.3.5-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fkdk`
--

-- --------------------------------------------------------

--
-- Table structure for table `alif`
--

CREATE TABLE `alif` (
  `id_alif` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `isi` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `created` varchar(20) NOT NULL,
  `upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alif`
--

INSERT INTO `alif` (`id_alif`, `judul`, `kategori`, `isi`, `image`, `created`, `upload`, `update`) VALUES
(2, 'RMDK', '', 'SELAMAT', '', '', '2019-10-09 10:46:02', NULL),
(3, 'sss', '', 'sss', 'alif-191009-44b5b21bbf.jpg', '', '2019-10-09 11:54:09', NULL),
(4, 'zzz', '', 'sss', 'alif-191009-ee4f13bf59.jpg', 'ddddd', '2019-10-09 11:55:21', NULL),
(5, 'rasdi', '', 'rasdi', 'alif-191009-007e82cfaa.jpg', '', '2019-10-09 11:57:01', NULL),
(6, 'sssss', '', 'ssss', 'notebook_laptop_technical_service-512.png', '', '2019-10-09 11:57:49', NULL),
(7, 'sssss', '', 'ssss', 'notebook_laptop_technical_service-512.png', '', '2019-10-09 11:59:43', NULL),
(8, 'ffff', '', 'fff', 'alif-191009-1d98d3de3b.jpg', '', '2019-10-09 12:01:50', NULL),
(9, 'jalan panjang', '', 'dddd', 'sertivikat_svg.png', '', '2019-10-11 10:36:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `Npm` int(30) NOT NULL,
  `Nama` varchar(30) NOT NULL,
  `No_Telp` int(15) NOT NULL,
  `Prodi` varchar(30) NOT NULL,
  `Fakultas` varchar(30) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.jpg'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`Npm`, `Nama`, `No_Telp`, `Prodi`, `Fakultas`, `image`) VALUES
(323, 'adas', 107, 'da', 'das', '323.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `mentoring`
--

CREATE TABLE `mentoring` (
  `id_mentoring` int(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `npm` varchar(50) NOT NULL,
  `prodi` varchar(50) NOT NULL,
  `fakultas` varchar(50) NOT NULL,
  `jadwal` datetime NOT NULL,
  `hobi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mentoring`
--

INSERT INTO `mentoring` (`id_mentoring`, `nama`, `npm`, `prodi`, `fakultas`, `jadwal`, `hobi`) VALUES
(1, 'rasdi', '15120', 'Teknik Informatika', 'Ilmu Komputer', '2019-10-24 00:00:00', 'footsall');

-- --------------------------------------------------------

--
-- Table structure for table `reference`
--

CREATE TABLE `reference` (
  `id_ebook` int(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `ket` text NOT NULL,
  `file` varchar(100) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reference`
--

INSERT INTO `reference` (`id_ebook`, `nama`, `ket`, `file`, `created`, `updated`) VALUES
(14, 'RMDK', 'risalah', 'reference-191009-3c7ca45374.jpg', '2019-10-09 10:12:43', NULL),
(15, 'sdd', 'dd', 'reference-191009-c36872b7a0.jpg', '2019-10-09 10:41:49', NULL),
(16, 'sdd', 'ss', 'reference-191009-985f8b18bc.jpg', '2019-10-09 10:51:55', NULL),
(17, 'sdd', 'sebuah buku', 'reference-191009-7b3b0e04ed.jpg', '2019-10-09 10:52:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_session`
--

CREATE TABLE `tbl_session` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_session`
--

INSERT INTO `tbl_session` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('062k03bestv72e6lu8076pim3f8n52e8', '127.0.0.1', 1562371312, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536323337313330313b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a227261736469223b757365725f706173737c733a33323a226163633766393039633133613534353930383636326539653066353761383361223b757365725f6e616d617c733a353a227261736469223b),
('0o8a7rbq0oncgqsb4jf3g8s7qf6gk9bt', '::1', 1568787643, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383738373633333b);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(12) NOT NULL,
  `image` varchar(100) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `no_telp`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(10, 'Rasdi Abdulrohman', 'ada@gmail.com', '085717059061', '4X6.jpg', '$2y$10$gQr7uR1qc003LK1RZnxchuc.GU07UMAK2ZvkrsM1N.BnUN9v/pKKS', 1, 1, 1562388998),
(15, 'rasdi Abdulrohman', 'admin@gmail.com', '6665', 'image.png', '$2y$10$RBww2lW9vHGxxDhW.86Pju8sHHkqlfGAS0BshKDhcKyQFlm45bF4O', 1, 1, 1570418588),
(16, 'mentor', 'mentor@gmail.com', '', 'Selection_084.png', '$2y$10$I1cKlVGSTyNZ/WfVKuTRneUWQsXzhhZSgijgITXEGRstQQt14jnA6', 3, 1, 1570420937),
(18, 'joko anwar', 'joko1@gmail.com', '', 'laptop-flat-design_1237-17.jpg', '$2y$10$4oblG9tU/taKt0jVqo717.YqGEmH.W43/.bleXQQ2o2tSumN2J2dC', 2, 1, 1570590051);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(4, 1, 3),
(6, 3, 2),
(9, 1, 6),
(14, 1, 7),
(15, 1, 4),
(19, 2, 2),
(27, 1, 8),
(28, 3, 7);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Member'),
(7, 'Mentor'),
(8, 'Reference');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member'),
(3, 'Mentor');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fa fa-dashboard', 1),
(2, 2, 'My profile', 'user', 'fa fa-users', 1),
(3, 2, 'Edit Profile', 'user/edit', 'fa fa-edit', 1),
(4, 3, 'Menu Manajement', 'menu', 'fa fa-building', 1),
(5, 3, 'Submenu Manajement', 'menu/submenu', 'fa fa-rss-square', 1),
(7, 1, 'Role', 'admin/role', 'fa fa-gears', 1),
(8, 4, 'Member Manajement', 'member', 'fa fa-database', 1),
(11, 7, 'Mentoring', 'mentor', 'fa fa-graduation-cap', 1),
(12, 7, 'Edit Mentoring', 'mentor/edit', 'fa fa-edit', 1),
(14, 8, 'Data Referensi', 'reference', 'fa fa-file-pdf-o', 1),
(15, 2, 'Referensi Buku', 'user/buku', 'fa  fa-book', 1),
(16, 2, 'ALIF', 'user/alif', 'fa fa-file-text', 1),
(17, 2, 'Create ALIF', 'user/add', 'fa fa-file-pdf-o', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alif`
--
ALTER TABLE `alif`
  ADD PRIMARY KEY (`id_alif`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`Npm`);

--
-- Indexes for table `mentoring`
--
ALTER TABLE `mentoring`
  ADD PRIMARY KEY (`id_mentoring`);

--
-- Indexes for table `reference`
--
ALTER TABLE `reference`
  ADD PRIMARY KEY (`id_ebook`);

--
-- Indexes for table `tbl_session`
--
ALTER TABLE `tbl_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alif`
--
ALTER TABLE `alif`
  MODIFY `id_alif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `mentoring`
--
ALTER TABLE `mentoring`
  MODIFY `id_mentoring` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reference`
--
ALTER TABLE `reference`
  MODIFY `id_ebook` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

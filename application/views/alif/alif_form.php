<section class="content-header">
      <h1>
       alif
        <small>Katagori Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href ="#"><i class= "fa fa-dashboard"></i>></a></li>
        <li class="active">alif</li>
          </ol>
     </section>

     
<section class="content">

  <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?=ucfirst($page)?> </h3>
     
            <div class="pull-right">
            <a href="<?= base_url('user')?>" class = "btn btn-primary btn-flat">
              <i class = "fa fa-undo">Back</i>
            </a>
          </div>

      <div class="box-body">
        <div class="row">
          <div class="col-md-4 col-md-offset-4">
          
                     <?php echo form_open_multipart ('user/process')?>
                        <div class="form-group">
                          <label> Judul *</label>
                          <input type="hidden" name="id" value="<?=$row->id_alif?>">
                          <input type="text" name="judul" value="<?= $row->judul?>" class="form-control" required>
                        </div>

                         <div class="form-group">
                          <label> Isi *</label>
                          <textarea name="isi" value="<?= $row->isi?>" class="form-control" required></textarea>
                        </div>

                        <div class="form-group">
                          <label> Gambar</label>
                          <?php if($page== 'edit'){
                            if($row->file !=null) {?>
                              <div>
                                <img src="<?= base_url('uploads/produk/'.$row->image)?>" style="width: 80px">
                              </div>
                              <?php
                            }
                          }?>
                          <input type="file" name="file" class="form-control">
                          <small>(biarkan kosong jika tidak <?=$page=='edit' ? 'diganti':'ada'?>)</small>
                        </div>


                        <div class="form-group">
                          <button type="submit" name="<?=$page ?>" class="btn btn-success btn-flat">Save</button>
                          <button type="reset" class="btn btn-flat">Reset</button>
                        </div>

                      </form></div>
        
        </div>

       </div>
       
      </div>

      </section>
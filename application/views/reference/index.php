    <section class="content-header">
      <h1>
       reference
        <small>reference Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href ="#"><i class= "fa fa-dashboard"></i>></a></li>
        <li class="active">reference </li>
          </ol>
     </section>


     <section class="content">


      <?php $this->view('message') ?>
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"> reference </h3>
          <div>
            <div class="pull-right">
            <a href="<?= base_url('reference/add')?>" class = "btn btn-primary btn-flat">
              <i class = "fa fa-users">Create</i>
            </a>
          </div>

        </div>

      <div class="box-body table-responsive">
       </div>
       

        <table class="table table-bordered table-stripped" id="table1">
          <thead>
              <tr>
                <th>#</th>
                <th>Nama</th>
                <th>Keterangan</th>
                <th>File</th>
                <th>Action</th>

              </tr>

          </thead>
          <?php $no = 1;
          foreach ($row->result() as $key => $data) { ?>

          <tbody>
            <tr>
              <td style="width: 5%;"><?=$no++?></td>
              <td><?= $data->nama?></td>
              <td><?= $data->ket?></td>
              <td>
                <?php if($data->file !=null) {?>
                
               <img src="<?= base_url('uploads/produk/'.$data->file)?>" style="width: 60px">
                  <?php }?>
                </td>


              
              <td class= "text-center" width="160 px" >
               
                  <a href="#" class = "btn btn-primary btn-xs">
                  <i class = "fa fa-pencil">Update</i>
                </a>
                
                <a href="#" onclick="return confirm('Apakah anda akan menghapus?')" class = "btn btn-danger btn-xs">
                  <i class = "fa fa-trush">Delete</i>
                </a>

              

              </td>

            </tr>
         <?php 
       } ?>
          </tbody>

        </table>

        </div>




      </div>

      </section>
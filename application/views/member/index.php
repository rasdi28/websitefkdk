
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3  text-gray-800">Daftar Member</h1>

          <div class="row">
          	<div class="col-lg">

          		<table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Email</th>
      <th scope="col">No Telp</th>
      <th scope="col">Member</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $i= 1;?>
  	<?php foreach($name as $nm): ?>
    <tr>
      <th scope="row"><?= $i;?></th>    
      <td><?= $nm['name'];?> </td>
      <td><?= $nm['email'];?></td>
      <td><?= $nm['no_telp'];?></td>
      <td><?= date('d F Y', $nm['date_created']);?>
      <td class= "text-center" width="160 px" >
               
                  <a href="">
                  <i class = "fa fa-pencil">Update</i>
                </a>
                
                <a href="" onclick="return confirm('Apakah anda akan menghapus?')">
                  <i class = "fa fa-trush">Delete</i>
                </a>


      </td>
    </tr>
    <?php $i++; ?>
    <?php endforeach; ?>
    
  </tbody>
</table>

          	</div>



          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('alif_m');
		
	}
	
	public function index()
	{
		$data['title']= 'My Profile';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','user/index',$data);

		
	}

	public function edit()
	{
		$data['title']= 'Edit Profile';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();

		$this->form_validation->set_rules('name',' Full Name', 'required|trim');


		if($this->form_validation->run() == false){
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','user/edit', $data);
		

		} else{
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$no_telp = $this->input->post('no_telp');

			//cek jika da gambar yang akan di upload

		$upload_image = $_FILES['image']['name'];
		
			if ($upload_image){
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']     = '2048';
				$config['upload_path'] = './assets/img/profile/';

				$this->load->library('upload', $config);

				 if ($this->upload->do_upload('image')){
				 	$old_image = $data['user']['image'];
				 	if($old_image != 'default.jpg') {
				 		unlink(FCPATH . 'assets/img/profile/' . $old_image);
				 	}



				 	$new_image = $this->upload->data('file_name');
				 	$this->db->set('image', $new_image);

				 }else{
				 	echo $this->upload->display_errors();
				 }



			}


			$this->db->set('name',$name);
			$this->db->set('no_telp',$no_telp);
			$this->db->where('email', $email);
			$this->db->update('user');

			$this->session->set_flashdata('message','<div class="alert alert-success" role="alert"> Your Profile Has been update
			</div>');
			redirect('user');

		}
		

	}

	public function buku(){

		$data['title']= 'Referensi Buku';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$this->load->model('alif_m');
		$data['row'] = $this->alif_m->get();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','user/buku',$data);
	}

	public function alif(){
		$data['title']= 'My ALIF';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$data['row'] = $this->alif_m->get();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','alif/alif_data',$data);
	}

	public function create(){
		$data['title']= 'My ALIF';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$data['row'] = $this->alif_m->get();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','alif/alif_form',$data);

	}

		
		public function add()
	{
		
		
		$alif = new stdClass();
		$alif ->id_alif = null;
		$alif->judul = null;
		$alif->isi = null;
		

		$data = array (
			'page' => 'add',
			'row' => $alif

		);

		$data['title']= 'alif';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','alif/alif_form',$data);

		
		
	}

	public function process(){

		$config['upload_path']     = './uploads/produk';
        $config['allowed_types']   = 'gif|jpg|pdf|jpeg';
        $config['max_size']        = 2048;
        $config['file_name']       = 'alif-'.date('ymd').'-'.substr(md5(rand()),0,10);
        $this->load->library('upload', $config);
        $post = $this->input->post (null, TRUE);
		if(isset($_POST['add'])) 
		{
        	if(@$_FILES['file']['name'] != null){
        	if($this->upload->do_upload('file')){

        	}
        	$post['file'] = $this->upload->data('file_name');
        	$this->alif_m->add($post);

        }
		
		}else if (isset($_POST['edit'])){
			$this->alif_m->edit($post);
		}

		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('success','data berhasil disimpan');
		}
		redirect('user/alif');

	}


}
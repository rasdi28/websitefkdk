<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mentor extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('mentoring_m');
	}

	public function index()
	{	
		$data['title']= 'Mentor';
		$data['row'] = $this->mentoring_m->get();
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','mentoring/index',$data);
		
	}
}
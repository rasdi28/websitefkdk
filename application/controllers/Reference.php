<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class reference extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model('reference_m');
	}
	
	public function index()
	{
		$data['title']= 'Reference';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$data['row'] = $this->reference_m->get();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','reference/index',$data);

		
	}
	
	public function add()
	{
		
		
		$reference = new stdClass();
		$reference ->id_ebook = null;
		$reference->nama = null;
		$reference->ket = null;
		

		$data = array (
			'page' => 'add',
			'row' => $reference

		);

		$data['title']= 'Reference';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->load->view('reference/reference_form',$data);
		
		
	}

	public function process(){

		$config['upload_path']     = './uploads/produk';
        $config['allowed_types']   = 'gif|jpg|pdf|jpeg';
        $config['max_size']        = 2048;
        $config['file_name']       = 'reference-'.date('ymd').'-'.substr(md5(rand()),0,10);
        $this->load->library('upload', $config);
        $post = $this->input->post (null, TRUE);
		if(isset($_POST['add'])) 
		{
        	if(@$_FILES['file']['name'] != null){
        	if($this->upload->do_upload('file')){

        	}
        	$post['file'] = $this->upload->data('file_name');
        	$this->reference_m->add($post);

        }
		
		}else if (isset($_POST['edit'])){
			$this->reference_m->edit($post);
		}

		if($this->db->affected_rows()>0){
			$this->session->set_flashdata('success','data berhasil disimpan');
		}
		redirect('reference');

	}


}
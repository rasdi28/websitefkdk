<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		is_logged_in();

		
	}

	public function index()
	{
		$data['title']= 'Member';
		$data['user'] = $this->db->get_where('user',['email'=>$this->session->userdata('email')])->row_array();
		$data['name'] = $this->db->get('user')->result_array();
		$this->load->view('template/header', $data);
		$this->load->view('template/sidebar', $data);
		$this->template->load('template/footer','member/index',$data);
		
	}

	

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alif_m extends CI_Model {

	public function get($id= null)
	{
		
		$this->db->from('alif');
		if ($id != null){
			$this->db->where('id_alif', $id);
		}
		$this->db->order_by ('judul','asc');
		$query = $this->db->get();
		return $query;
	}

	
public function del($id)
	{
		$this->db->where('item_id', $id);
		$this->db->delete('p_item');
	}

public function add($post)
	{
		$params = [
			
			'judul'=> $post['judul'],
			'isi'=>$post['isi'],
			'image'=>$post['file'],
			
		];
		$this->db->insert('alif', $params);
	}


	public function edit($post)
	{
		$params = [
			'barcode'=>$post['barcode'],
			'name'=> $post['item_name'],
			'category_id'=>$post['category'],
			'unit_id'=>$post['unit'],					
			'price'=>$post['price'],	
			
			'updated'=> date('Y-m-d H:i:s'),




		];
		if($post['image'] != null){
			$params['image']= $post['image'];
		}
		$this->db->where('item_id',$post['id']);
		$this->db->update('p_item', $params);
	}


	function check_barcode($code, $id= null){
		$this->db->from('alif');
		$this->db->where('judul', $code);
		if($id != null){
			$this->db->where('judul !=', $id );
		}
		$query = $this->db->get();
		return $query;
	}

	

}

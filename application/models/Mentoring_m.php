<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mentoring_m extends CI_Model {

	public function get($id= null)
	{
		$this->db->from('mentoring');
		if ($id != null){
			$this->db->where('id_mentoring', $id);
		}
		$query = $this->db->get();
		return $query;
	}

}


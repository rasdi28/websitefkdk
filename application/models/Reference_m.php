<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reference_m extends CI_Model {

	public function get($id= null)
	{
		
		$this->db->from('reference');
		if ($id != null){
			$this->db->where('id_ebook', $id);
		}
		$this->db->order_by ('nama','asc');
		$query = $this->db->get();
		return $query;
	}

	
public function del($id)
	{
		$this->db->where('item_id', $id);
		$this->db->delete('p_item');
	}

public function add($post)
	{
		$params = [
			
			'nama'=> $post['nama'],
			'ket'=>$post['keterangan'],
			'file'=>$post['file'],
			
		];
		$this->db->insert('reference', $params);
	}


	public function edit($post)
	{
		$params = [
			'barcode'=>$post['barcode'],
			'name'=> $post['item_name'],
			'category_id'=>$post['category'],
			'unit_id'=>$post['unit'],					
			'price'=>$post['price'],	
			
			'updated'=> date('Y-m-d H:i:s'),




		];
		if($post['image'] != null){
			$params['image']= $post['image'];
		}
		$this->db->where('item_id',$post['id']);
		$this->db->update('p_item', $params);
	}


	function check_barcode($code, $id= null){
		$this->db->from('reference');
		$this->db->where('name', $code);
		if($id != null){
			$this->db->where('name !=', $id );
		}
		$query = $this->db->get();
		return $query;
	}

	

}
